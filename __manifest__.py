# -*- coding: utf-8 -*-

{
    'name': "Real Estate Management",
    'summary': """Demo Real Estate Management for Grand City Property""",
    'author': "Viet Pham, v.pham@qvpham.de",
    'website': "http://www.qvpham.de/",
    'category': 'Custom',
    'version': '0.1.0',
    'depends': [
        "base",
        "contacts"
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/building_view.xml',
        'views/apartment_view.xml',
        'views/contract_view.xml',
        'views/menu.xml',
    ],
    'demo': [
    ],
    'installable': True,
}
