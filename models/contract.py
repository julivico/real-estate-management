import logging

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Contract(models.Model):
    _name = 'gcp.contract'

    tenant = fields.Many2one(comodel_name='res.partner', required=True,
                             help='The Partner that is renting the building or apartment')
    building_id = fields.Many2one(comodel_name='gcp.building', string='Building', required=True)
    apartment_id = fields.Many2one(comodel_name='gcp.apartment', string='Apartment',
                                   domain="[(('rented', '=', False)]")
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End Date', help='Empty end date makes the contract unlimited')

    @api.onchange('building_id')
    def onchange_building_id(self):
        if self.building_id:
            return {'domain': {'apartment_id': [('building_id', '=', self.building_id.id),
                                                ('rented', '=', False)]}}
        return {'domain': {'apartment_id': [('rented', '=', False)]}}
