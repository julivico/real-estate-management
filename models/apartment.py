import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Apartment(models.Model):
    _name = 'gcp.apartment'

    name = fields.Char(string='Name', store=True, readonly=True)
    building_id = fields.Many2one(string='Building', required=True, comodel_name='gcp.building')
    area = fields.Float(string='Area', required=True)
    rented = fields.Boolean(string='Rented', compute='_compute_rented', store=True,
                            help='''* True - if there is a rent contract defined for the Apartment
* False - otherwise''')
    contract_ids = fields.One2many(string='Contract', comodel_name='gcp.contract', inverse_name='apartment_id')
    is_created = fields.Boolean(string='Is created')

    @api.depends('contract_ids')
    def _compute_rented(self):
        for record in self:
            rented = False
            if record.contract_ids and len(record.contract_ids) > 0:
                rented = True
            record.rented = rented

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, vals):
        if vals and not vals.get('name', None) and vals.get('building_id', None):
            building = self.env['gcp.building'].browse(vals.get('building_id'))
            index = len(building.apartment_ids) + 1
            computed_name = '{}_{}'.format(building.name, index)
            vals['name'] = computed_name
        vals['is_created'] = True
        return super(Apartment, self).create(vals)
