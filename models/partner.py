import logging

from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Partner(models.Model):
    _inherit = 'res.partner'

    gcp_contract_ids = fields.One2many(comodel_name='gcp.contract', inverse_name='tenant')
