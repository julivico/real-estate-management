import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class Building(models.Model):
    _name = 'gcp.building'

    name = fields.Char(string='Name', required=True)
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char()
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    rental_area = fields.Float(string='Rental Area', compute='_compute_rental_area', store=True, readonly=True,
                               help='SUM of areas of all Apartments in the Building')
    apartment_ids = fields.One2many(string='Apartments', help='List of apartments',
                                    comodel_name='gcp.apartment', inverse_name='building_id')
    contract_ids = fields.One2many(string='Contract', comodel_name='gcp.contract', inverse_name='building_id')

    @api.depends('apartment_ids', 'apartment_ids.area')
    def _compute_rental_area(self):
        for record in self:
            rental_area = 0
            if record.apartment_ids:
                for apartment in record.apartment_ids:
                    if apartment.area and apartment.area > 0:
                        rental_area += apartment.area
            record.rental_area = rental_area

    _sql_constraints = [
        ('unique_building_name', 'unique (name)', 'Building name must be unique!')
    ]
